# Prueba Técnica Desarrollador Front-End

## Descripción
El proyecto consta de una única página donde se encuentra un formulario donde rellenar los datos, una tarjeta de crédito, los datos de este formulario se irán reflejando dinámicamente en la tarjeta replicada justo arriba, una vez estén todos los datos correctamente validados se podrán agregar los datos introducidos a una lista de tarjetas que aparecen debajo donde podrás gestionarlas.
 
## Pre-requisitos
- Instalar [Node.js](https://nodejs.org/en/)


## Iniciar el proyecto
- Clonar el repositorio
```
git clone https://gitlab.com/keilagonzalezduval/prueba_sii.git
```
- Instalar las dependencias
```
cd prueba_sii
npm install
```
- Correr el proyecto
```
npm run dev
```
  Acceder a `http://localhost:3000` (o tu puerto predefinido)

## Información técnica
El proyecto está hecho en Node.js usando los siguientes módulos:
- ***express***: Para la gestión de las rutas y servicios.
- ***firebase-admin***: Para la conexión con Firebase y la estructuración del web API.
- ***express-handlebars***: El view engine para poder usar los datos recogidos de la bbdd en el front de la aplicación.
- ***morgan***: Para tener control y registros de las conexiones y lo que pasa en la aplicación por consola.

## API
Para la gestión de datos se usa Firebase (Realtime Database).

_/_ : Es la página inicial, en esta se visualiza el listado de las tarjetas guardadas. ***READ***

_/new-card_ : Recoge los datos enviados a través del formulario y los escribe en la bbdd. ***CREATE***

_/delete-card/:key_ : Recibe la clave única de la tarjeta seleccionada y la borra. ***DELETE***

_/update-card/:key_ : Recibe la clave única y recoge los nuevos datos del objeto y los actualiza, esta operación no está implementada gráficamente. ***UPDATE***

## Observaciones
Del primer bloque de requerimientos, no están implementados exactamente los puntos referentes a los mensajes de error en caso de que algo no haya ido bien (1.g y 2.c), ya que bootsrap ya incluye su propia manera de alertar en casos inválidos.