const {Router} = require("express");
const router = Router();

//firebase conf
const admin = require('firebase-admin');
var serviceAccount = require("../../prueba-sii-firebase-adminsdk-1c4j9-675fc07d30.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://prueba-sii-default-rtdb.firebaseio.com"
});

const db = admin.database();

// main page (READ)
router.get("/", (req, res) => {
    db.ref('cards').once('value',(snapshot) =>{
        const data = snapshot.val();
        res.render("index",{cards: data});
    }); 
});

// receiving form data (CREATE)
router.post("/new-card", (req, res) => {
    //masking the card number
    let masked = "";
    for (let i = 0; i < req.body.cardNumber.length; i++) {
        let letter = req.body.cardNumber.substring(i, i + 1);
        if (i > 1 && i < 12) {
            letter = "*";
        }
        masked += letter;
    }

    //getting the incremental id
    let newId = 0;
    db.ref('lastId').once('value', (snapshot) => {
        newId = snapshot.val() + 1;

        const newCard = {
            id: newId,
            cardNumber: req.body.cardNumber,
            masked: masked,
            date: req.body.date,
            name: req.body.name,
            cvv: req.body.cvv
        }
        db.ref("cards").push(newCard);        
        db.ref("lastId").set(newId);
        console.log(newCard);

        res.redirect("/");
    }); 
});

// deleting the selected card (DELETE) 
router.get("/delete-card/:key", (req, res) => {
   db.ref("cards/" + req.params.key).remove();
   res.redirect("/");
});

// updating the selected card (UPDATE) (not implemented)
router.post("/update-card/:key", (req, res) => {
    const {id, cardNumber, masked, date, name, cvv } = req.body;
    db.ref("cards/" + req.params.key).update({id, cardNumber, masked, date, name, cvv });
    res.redirect("/");
});

module.exports = router;