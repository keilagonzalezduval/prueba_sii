//change the name dynamically
function changeName() {
	//clean numbers
	let input = document.getElementById("nameInput").value
        .replace(/[0-9]/g, '');
        
	document.getElementById("nameStr").textContent = input.toUpperCase();

	if(input == ''){
		document.getElementById("nameStr").textContent = 'DONALD FLINCH CORTEZ';
	}
};

//change the card number dynamically
function changeNumber() {
	//clean blank spaces and letters, add space every 4 digits 
	let input = document.getElementById("numberInput").value
        .replace(/\s/g, '')
        .replace(/\D/g, '')
        .replace(/([0-9]{4})/g, '$1 ')
        .trim();
    
    if (input.length > 18){
        document.getElementById("numberInput").blur();
    }

    document.getElementById("numberStr").textContent = input;

	if(input == ''){
		document.getElementById("numberStr").textContent = '5375 4411 4540 0954';
	}
};

//change the exp date dynamically
function changeDate() {
	let input = document.getElementById("dateInput").value;
    //.replace(/([0-9]{2})/g, '$1/')

	document.getElementById("dateStr").textContent = input.toUpperCase();

	if(input == ''){
		document.getElementById("dateStr").textContent = '06/24';
	}
};

//set default values in card on reset
function clean() {
    document.getElementById("numberStr").textContent = '5375 4411 4540 0954';
    document.getElementById("dateStr").textContent = '06/24';
    document.getElementById("nameStr").textContent = 'DONALD FLINCH CORTEZ';
}